import hello
import unittest


class TestHome(unittest.TestCase):

    def setUp(self):
        hello.app.testing = True
        self.app = hello.app.test_client()

    def test_home(self):
        result = self.app.get('/')


if __name__ == '__main__':
    unittest.main()
