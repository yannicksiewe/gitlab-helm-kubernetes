{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ghk.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate to 63 characters because it's maximum length of a "label" follow by kubernetes (RFC-952 and RFC-1123).
*/}}
{{- define "ghk.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- tpl .Values.fullnameOverride $ | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/* Create chart name and version as used by the chart label. */}}
{{- define "ghk.labels" -}}
app: "{{ template "ghk.fullname" . }}"
helm.sh/chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
app.kubernetes.io/instance: {{ .Release.Name | quote }}
app.kubernetes.io/name: "{{ template "ghk.name" . }}"
app.kubernetes.io/version: "{{ .Chart.AppVersion }}"
{{- end -}}

{{/* matchLabels */}}
{{- define "ghk.matchLabels" -}}
release: {{ .Release.Name }}
app: "{{ template "ghk.fullname" . }}"
{{- end -}}

