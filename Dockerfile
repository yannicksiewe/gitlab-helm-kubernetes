FROM python:3.6-alpine
MAINTAINER  yannick.siewe@gmail.com

WORKDIR /src
COPY . /src
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5555

CMD ["python", "./hello.py"]
